# PixelUI
**PixelUI is based on Stock** 
- > (Pixel 3, Pixel 3 XL, Pixel 3a, Pixel 3a XL, Pixel 4, Pixel 4 XL, Pixel 4a, Pixel 4a 5G, Pixel 5, Pixel 5a 5G, Pixel 6, Pixel 6 Pro).

**PixelUI Ports For Redmi Note 8/8T**
![final](https://user-images.githubusercontent.com/37813398/162584397-0a8b3398-ab21-4997-997d-355decedadad.jpg)
**Android 12 QPR3**
- >[PixelUI_Ginkgo-12.0-20220409-8421.zip](https://drive.google.com/uc?id=12AS76M0ka46ZOEdkkxcX-v4mDH4LyFf6&export=download)
- >[PixelUI_Ginkgo-12.0-20220314-8418.zip](https://drive.google.com/uc?id=1A5VrqLRBKvqMaB6hTbju32RzYxYJvm-0&export=download)
- >[PixelUI_Ginkgo-12.0-20220310-8410.zip](https://drive.google.com/uc?id=1ijwW-UmK0wI8dlF7tqPM-jFb1JuQj44I&export=download)

**Android 12**
- >[PixelUI_Ginkgo-12.0-20220219-0931.zip](https://drive.google.com/uc?id=1e0nF2SoO-0LVXQWbyAvYbIPODeUrAnFn&export=download)
- >[PixelUI_Ginkgo-12.0-20220209-1908.zip](https://drive.google.com/uc?id=1GkYBtW_8VnyhRSZqeAGI2oDAZ-oPLHbp&export=download)

**Android 12.L**
- >[PixelUI_Ginkgo-12.L-20220407-0339.zip](https://drive.google.com/uc?id=1HrYkVbOK4jk6zst-jgJcqhKbILsvOVSC&export=download)
- >[PixelUI_Ginkgo-12.L-20220406-0338.zip](https://drive.google.com/uc?id=1WWOFjP-t0AkEfvgK6REGisSijMJtoIrn&export=download)
- >[PixelUI_Ginkgo-12.L-20220405-0337.zip](https://drive.google.com/uc?id=1Y0gyKDoylMr74s2qCRGiep4DSjKgKO0Z&export=download)
- >[PixelUI_Ginkgo-12.L-20220309-0335.zip](https://drive.google.com/file/d/19Wiuagqanib2Q_-trKbq9OsiSWm0hQTE/view)
- >[PixelUI_Ginkgo-12.L-20220225-0165.zip](https://drive.google.com/file/d/1yfO9cQRoK1XLZ9ZrEn7QKU3WvrDYJzon/view?usp=drivesdk)
- >[PixelUI_Ginkgo-12.L-20220210-1970.zip](https://drive.google.com/uc?id=1IJZDy8PBtW-uKwzjKWiSW0LCOXygny1y&export=download)
- >[PixelUI_Ginkgo-12.L-20220127-1075.zip](https://drive.google.com/uc?id=18SfYWaPSc_j3d3aA1Nte8PO5cVEd5bE7&export=download)
- >[PixelUI_Ginkgo-12.L-20220113-1787.zip](https://drive.google.com/file/d/10M_jchJFuMX1UXJJnaGB-sU0WOJ_tlMs/view?usp=drivesdk)
- >[PixelUI_Ginkgo-12.L-20211222-1907.zip](https://drive.google.com/file/d/1KVI4n2nor0M_IKjmzePl3nU3YjZ8L9cY/view?usp=drivesdk)


**Android 13**
- >[PixelUI_Ginkgo-13.0-20220325-3036.zip](https://drive.google.com/uc?id=1fibdEAEBy6vx0-nVGX2ncf3G0_wE0Bal&export=download)
- >[PixelUI_Ginkgo-13.0-20220321-3032.zip](https://drive.google.com/uc?id=1oOgbYthTAVW7pVPRbNkLxYsPl9ZRtqVx&export=download)
- >[PixelUI_Ginkgo-13.0-20220218-0100.zip](https://drive.google.com/uc?id=1aMMMHTQFZvPXavQFtsR9QnXG0XoNaOhd&export=download)
- >[PixelUI_Ginkgo-13.0-20220211-0182.zip](https://drive.google.com/uc?id=1ACHdqvf2NK0Rql2Ul1LMmlL5wrAwWztl&export=download)
